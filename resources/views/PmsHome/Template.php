@include('PmsHome.Head')
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        @include('PmsHome.Header')
        @include('PmsHome.LeftSide')
    </body>
    @include('PmsHome.Footer')
    @include('PmsHome.RightSide')
    @include('PmsHome.BottomCss')
</html>
