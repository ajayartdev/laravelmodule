<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   // return view('welcome');
   // return view('html.user_login');
    return view('auth.register');
});



Auth::routes();
Route::get("demo", "UserController@tester");
Route::get("home", "UserController@home");
Route::get("property", "UserController@property");
Route::get("dashboard", "UserController@dashboard");
Route::get("user_login", "UserController@user_login");
Route::get("user_sign", "UserController@user_sign");
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/auth' , function() {
	if(!Auth::check())
	{
		$user = App\User::find(1);
		Auth::login($user);
	} 
	return Auth::User();
});
