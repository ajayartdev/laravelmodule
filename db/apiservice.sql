-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2019 at 02:26 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apiservice`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('018ac35ee273e871ec177e1b9d5e52a8653ac120f141cb61aaec934220b55e9bd674bb2c0f85754e', 1, 1, 'MyApp', '[]', 0, '2019-01-31 06:08:21', '2019-01-31 06:08:21', '2020-01-31 11:38:21'),
('0f1f585e8d93c4bfeb281a1dc0c6a11789129c5c8729f2dcab110a8904d5f0183053ac3f9baf6c2d', 1, 1, 'MyApp', '[]', 0, '2019-01-30 01:25:18', '2019-01-30 01:25:18', '2020-01-30 06:55:18'),
('1cfafb71b9bfa2085ebde30b870e4b5489de7e0715166347a4292b712e8304f8434e9e6375a8bf89', 1, 1, 'MyApp', '[]', 0, '2019-01-31 04:16:46', '2019-01-31 04:16:46', '2020-01-31 09:46:46'),
('22a2fb33cca5f59489edc4e6f7fc9cb683d3a798b015f5fe37cda542c3da824174fb4cefd90e3d8d', 2, 1, 'MyApp', '[]', 0, '2019-01-30 02:45:40', '2019-01-30 02:45:40', '2020-01-30 08:15:40'),
('2418626f097107a8f402e8dd75c154928877781da8b538a1d793235728c52d3eaa3bea399d25dd31', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:50:14', '2019-01-31 00:50:14', '2020-01-31 06:20:14'),
('249c82da2bd60545ab39da2c3baab6789cb7b372be0d81aabfdb2bfb54f88aa859bc33dc73402f08', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:07:49', '2019-01-31 01:07:49', '2020-01-31 06:37:49'),
('26f1493414b3c4e604825167c4e4dbe153317bc343d492cb80eba4f5ff111e54cac3317a95c5573b', 1, 1, 'MyApp', '[]', 0, '2019-01-31 04:15:05', '2019-01-31 04:15:05', '2020-01-31 09:45:05'),
('281b1e9c66132cd6566836120630e3a75010b035daec843caa283f03bb4242ef7c314bbec79e3965', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:07:49', '2019-01-31 01:07:49', '2020-01-31 06:37:49'),
('28cb2418b5621857e885bbbd2e08c365ad6d80f5c579b7477ece8056a5280b4fdad38f39c997cf96', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:07:35', '2019-01-31 01:07:35', '2020-01-31 06:37:35'),
('2a781b32fbe80c1da76555f519b397036ee49a69f9297c51f7768f7a964209d9cdd99a2cfaa3d2d1', 1, 1, 'MyApp', '[]', 0, '2019-01-30 04:39:52', '2019-01-30 04:39:52', '2020-01-30 10:09:52'),
('2b2f6ea9a1550f77e959283fd6c30722a95bf9dc6afda3feb0c9a48c86675276f8c9578fc736eeb6', 1, 1, 'MyApp', '[]', 0, '2019-01-31 04:00:45', '2019-01-31 04:00:45', '2020-01-31 09:30:45'),
('2dd1194f0278828f1bf7037a66a6a2fe393b09560e33fba17f22d6462461a474de7b06b6db839221', 1, 2, NULL, '[]', 0, '2019-02-02 00:58:34', '2019-02-02 00:58:34', '2020-02-02 06:28:34'),
('2f6c6cfb8318828c9430f5124997eb159bf204df4e134ab7b9b1f95aa39afde7d34db2f7bc48fcb8', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:55:26', '2019-01-31 00:55:26', '2020-01-31 06:25:26'),
('325887af14abc7ec6d93e42da365ca302d1dfd4a9c2dc497f87db1882b2a81bbb3519041640b6e07', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:54:18', '2019-01-31 00:54:18', '2020-01-31 06:24:18'),
('337ba0ca1640ab9e60f26f99fb25bc0a18df4a70ee712aa5355575f9dfdfaaf4923d8ceaebdcea8c', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:08:01', '2019-01-31 01:08:01', '2020-01-31 06:38:01'),
('3480d57eba355462295c85943e125bb94f077a21b2fbd1b6541a126783df150e4332a980d7f8041f', 1, 1, 'MyApp', '[]', 0, '2019-01-31 04:14:38', '2019-01-31 04:14:38', '2020-01-31 09:44:38'),
('395d83837a506a4eee4204e65760aa1cd109281b2c3bee6c193800e19aa17e3fa9de271e7edb780d', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:55:03', '2019-01-31 00:55:03', '2020-01-31 06:25:03'),
('3daad6565819d4af44671ecc3966b153659531206c8ca932f7ea6ac95a4087ab8a8f87fa9d971402', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:49:28', '2019-01-31 00:49:28', '2020-01-31 06:19:28'),
('3e281664c8a921b6330cf137d33e5d53bc74e52d4507cd4247b1878c616a3e7c4b24a47310d37625', 1, 1, 'MyApp', '[]', 0, '2019-02-01 00:40:00', '2019-02-01 00:40:00', '2020-02-01 06:10:00'),
('409dc2afa110da24da82ac2ab74ddcbbe5971ef84b7c35c5d604ece609d9df93e08b4d2189048358', 2, 1, 'MyApp', '[]', 0, '2019-01-30 02:42:29', '2019-01-30 02:42:29', '2020-01-30 08:12:29'),
('4498d205f77b8f3e3502a90bd164fcb8cbb64167b82324c9d90882ea65ccd4f99bb98d3a7b8ec7a3', 1, 1, 'MyApp', '[]', 0, '2019-01-30 08:21:34', '2019-01-30 08:21:34', '2020-01-30 13:51:34'),
('44e4cef42a5ffde69be78e1bd5977f05dcbed61d925c8921f1549aee2d85a0e05580714c8c8f89ca', 1, 1, 'MyApp', '[]', 0, '2019-01-31 03:17:48', '2019-01-31 03:17:48', '2020-01-31 08:47:48'),
('4680631c5ef6c8e327719e6da6b0973160318c5a7acb2cce59d18d60d0e2657d67a68fd1f79d613f', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:13:17', '2019-01-31 01:13:17', '2020-01-31 06:43:17'),
('4d9d0cf261a931f1cd5cd7726c396d3ddcad743ada9083ed4e9a820603594eef5a4bf2f183098ef5', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:08:16', '2019-01-31 01:08:16', '2020-01-31 06:38:16'),
('4f4618d4bf408b8dfa6937901e34a66a0b8301c65a8f618c45027a70e9d8f81c0751861d9316ceb1', 9, 1, 'MyApp', '[]', 0, '2019-01-30 08:44:10', '2019-01-30 08:44:10', '2020-01-30 14:14:10'),
('507f11cdcea0d8ae76a9c1ee72dbbf9a7a138446f0b3d9fe04de9cf30ba33b2248ed84717b7b0398', 1, 2, NULL, '[]', 0, '2019-02-02 01:21:11', '2019-02-02 01:21:11', '2020-02-02 06:51:11'),
('50bd5c5b1abbf08e2d4d0edc65eba8a30b50acf1e841f22a120c2a4308d246c82d5ef88add934160', 2, 1, 'MyApp', '[]', 0, '2019-01-30 02:41:06', '2019-01-30 02:41:06', '2020-01-30 08:11:06'),
('51b448cfc017994a3140a3b08f769fffe951330f17a3274debb0456d49c3929e527af5b68d279863', 2, 1, 'MyApp', '[]', 0, '2019-01-30 02:59:08', '2019-01-30 02:59:08', '2020-01-30 08:29:08'),
('53a0c63cd98c66aed4c005ee64bc64808c748607b3df640a03a86b3f36fd8e4c4ae82db9842c6340', 6, 1, 'MyApp', '[]', 0, '2019-01-30 08:20:17', '2019-01-30 08:20:17', '2020-01-30 13:50:17'),
('547ffb72af04a3a4b898531cde201641800579990f4469382d19ca38115d69c86cb55218d5102963', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:50:02', '2019-01-31 00:50:02', '2020-01-31 06:20:02'),
('576d3c9924232785ad2c4d0f10b04fd6a1cae957bd13be36c240c2ce7254b49afe937ca3b09aaf58', 1, 1, 'MyApp', '[]', 0, '2019-02-02 00:30:10', '2019-02-02 00:30:10', '2020-02-02 06:00:10'),
('5f935b887181803d6c47527075f4d8d6b6e032b652f290bfa0003c4b28fcd197a76f8d4448a80125', 1, 1, 'MyApp', '[]', 0, '2019-02-02 01:00:57', '2019-02-02 01:00:57', '2020-02-02 06:30:57'),
('6341307358c238526fbbdcad329f9e6cac9ccfd29b85903d4c78f36ee201d99cc1d136822eaed8cb', 1, 1, 'MyApp', '[]', 0, '2019-01-31 03:17:43', '2019-01-31 03:17:43', '2020-01-31 08:47:43'),
('65fe3a42df890b6e9c0df6df905b15522162fd469944fbc3cb7c6ac099d41b21d9f0951735b59d65', 3, 1, 'MyApp', '[]', 0, '2019-01-30 06:28:36', '2019-01-30 06:28:36', '2020-01-30 11:58:36'),
('68a8bc16c279ce00a1bfdcdf538b04c08a47f8f91c5a0845b4547115a7c49807d8a4d521b6b25721', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:55:15', '2019-01-31 00:55:15', '2020-01-31 06:25:15'),
('6a8b824bd04dcacf9c535a2d67a7385bc78ab7be86be653971218767ef2e39efd30987443f7f1f85', 8, 1, 'MyApp', '[]', 0, '2019-01-30 08:42:22', '2019-01-30 08:42:22', '2020-01-30 14:12:22'),
('6f2ff717022b0553c4a70d3e7446bbf49a425ef48b1cb9ebb39c67a429ad98e405f430157f8e1225', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:55:15', '2019-01-31 00:55:15', '2020-01-31 06:25:15'),
('76c9d26bd161e5c50e6d0a17884dc8456078c4eeff13221e620b7c0ad9a19228ffad92dcb3d0bcd0', 1, 1, 'MyApp', '[]', 0, '2019-01-31 03:18:03', '2019-01-31 03:18:03', '2020-01-31 08:48:03'),
('79269467e695e6a1ff150fd5d4afe099c3663fcefab220f49cbf7f63bebb4452c57716b2a4afbeee', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:50:02', '2019-01-31 00:50:02', '2020-01-31 06:20:02'),
('796801d486f82da5b04301256bba2d93f96dbadf885ecc86f241cb59e5b4847991d3800efd2525a2', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:48:28', '2019-01-31 00:48:28', '2020-01-31 06:18:28'),
('7fd8fcd5cc9fc639bcb2ce4e021ad826a713251074ca00470856115ef014831bc3c7a9dd3aa26300', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:07:35', '2019-01-31 01:07:35', '2020-01-31 06:37:35'),
('812d7273b4c718e06c74f88c1c29758ed0a284268da6b0d9e58616a61dbbafb5f4e49bad48d35cf4', 8, 1, 'MyApp', '[]', 0, '2019-01-30 08:42:03', '2019-01-30 08:42:03', '2020-01-30 14:12:03'),
('84b7eeb730a897f4f41af053225a8679fd1da22050c4a6fb98d9670229cf22914cb3a95cade6f927', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:07:03', '2019-01-31 01:07:03', '2020-01-31 06:37:03'),
('8788db5706844cbaeab4da492427606e28a4e9d59c2604d3c53599c41d9b840dd8e1146b50c643ad', 1, 2, NULL, '[]', 0, '2019-02-01 08:00:19', '2019-02-01 08:00:19', '2020-02-01 13:30:19'),
('88f00ed7f2c2d1f53b65ed9d50c0e15d1aaa65252017fb7e87ad184f4da712390f3363f7e78eef6a', 1, 1, 'MyApp', '[]', 0, '2019-01-31 03:19:15', '2019-01-31 03:19:15', '2020-01-31 08:49:15'),
('8f64081120c4120e48cd2abd17e2ee1ed95259c7e3815cd2957242f4a40b9657a608d78831b22765', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:11:12', '2019-01-31 01:11:12', '2020-01-31 06:41:12'),
('914493d2579afbc2cd6dcbefeb606671d3bb4f643ab5a3ad1a272b0d8dce7f48873ed613792aea43', 1, 1, 'MyApp', '[]', 0, '2019-02-02 00:30:54', '2019-02-02 00:30:54', '2020-02-02 06:00:54'),
('91a491765b83d9aea65609947de90cbcad24c94782bb6a33900c9c8e0e3561a71b172c10167d3bc0', 1, 1, 'MyApp', '[]', 0, '2019-01-30 06:20:20', '2019-01-30 06:20:20', '2020-01-30 11:50:20'),
('91cdd477ad4aa99a79b47b0ac1d8f31c7da09d1bcf94a4ad9008353e43a7c7c2d9fea4d34438a515', 1, 1, 'MyApp', '[]', 0, '2019-01-30 09:02:02', '2019-01-30 09:02:02', '2020-01-30 14:32:02'),
('928f082b7b97289536f62aa0a2e692ddc933ef582b0ba4530ff0e17d998eb532efa6895f55db77bf', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:06:18', '2019-01-31 01:06:18', '2020-01-31 06:36:18'),
('9372ddc2b63d00ca965fb775bb8a4644b5ec81c7f7801d5e65e6f5c24d1361ad2fab16ce023714ce', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:47:49', '2019-01-31 00:47:49', '2020-01-31 06:17:49'),
('98ae17177a181f4d886b735a8fb2b510797aeb3943257308184c7050714f3bdd1bf85413f30dfb29', 2, 1, 'MyApp', '[]', 0, '2019-01-30 02:55:13', '2019-01-30 02:55:13', '2020-01-30 08:25:13'),
('9de76bfd4e4189f3b27010792fd4a7918fe1ae3f7549c184c47481701d4a343404fb43458897f220', 1, 1, 'MyApp', '[]', 1, '2019-01-30 05:26:37', '2019-01-30 05:26:37', '2020-01-30 10:56:37'),
('a24911636dc5fccd3cc6e452768a666bcc521e1111a55cca9b768a94d3a1568128866c9895cac329', 1, 2, NULL, '[]', 0, '2019-02-01 08:21:46', '2019-02-01 08:21:46', '2020-02-01 13:51:46'),
('a6e417a4face4b5b29b790cfdbfccd0593994821c0e304269a95029440f95b0b9fb7021d9b89a033', 1, 1, 'MyApp', '[]', 0, '2019-01-30 08:31:19', '2019-01-30 08:31:19', '2020-01-30 14:01:19'),
('a9d1e8bd3637826de042f56d06bf2d078bc8af163ef69afa654688dbd5a55554ed71f90abefbb6b4', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:08:17', '2019-01-31 01:08:17', '2020-01-31 06:38:17'),
('ac538b47a342ea2b430167d436e79476a45ce111054ed768564b7963ab55158628e8382ae051b60a', 1, 2, NULL, '[]', 0, '2019-02-02 00:37:02', '2019-02-02 00:37:02', '2020-02-02 06:07:02'),
('af2e92158e0e2336700874870c630b910e167e73725ec87a52847a80890ea78ac40d70b8176cbf2f', 1, 1, 'MyApp', '[]', 0, '2019-01-30 04:37:43', '2019-01-30 04:37:43', '2020-01-30 10:07:43'),
('b02b9f7091d80061c098a7f6a1223690bff5cf4660c04e653bb246958ba594d26e70d246cc7d0995', 1, 1, 'MyApp', '[]', 1, '2019-01-30 04:41:26', '2019-01-30 04:41:26', '2020-01-30 10:11:26'),
('b3424dc0c733bc953fdf9c822893fa357de92376ed13c981ed1fb7a4884989ce03e5565677c5a637', 1, 1, 'MyApp', '[]', 0, '2019-01-31 04:52:18', '2019-01-31 04:52:18', '2020-01-31 10:22:18'),
('b3b1ab361b6281302f1e0b78712543fb541be12ef88ff9ad6992489036cd95a2e14c97ba719db646', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:07:03', '2019-01-31 01:07:03', '2020-01-31 06:37:03'),
('bb7e84036e2780bcf1de09a7c0a0bc1f552ed88846113570bd4e9a532906d8c762b176243b96a94b', 1, 1, 'MyApp', '[]', 0, '2019-01-30 08:01:57', '2019-01-30 08:01:57', '2020-01-30 13:31:57'),
('bc74ca0f08dde86aef573484ee4ea583dba67fc6c92095778c780c6d52e6dc0bcabd173669e14d2f', 1, 2, NULL, '[]', 0, '2019-02-02 02:38:45', '2019-02-02 02:38:45', '2020-02-02 08:08:45'),
('bf16aa5275ac00a7378b2a7b1f5d562d352463efee1c9d32d8d0874d285684a37b687300d228916d', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:06:15', '2019-01-31 01:06:15', '2020-01-31 06:36:15'),
('c6576727f0971e41c76c07bb64fe8883afadd00ba148f922e948c4bee39e2e2af562826d15da07b3', 4, 1, 'MyApp', '[]', 0, '2019-01-30 08:17:07', '2019-01-30 08:17:07', '2020-01-30 13:47:07'),
('c7aaa1a76e54324cf09f33ab02163dde5e2e55d0de5a07523c2b8eb0047c5f2ae1986aba491edf72', 2, 1, 'MyApp', '[]', 1, '2019-01-30 03:29:40', '2019-01-30 03:29:40', '2020-01-30 08:59:40'),
('c8b167894587c649b020e7edcdab9f0be17860580e167e0dd2e93105fa4eb38103d80b06beb4a07d', 1, 1, 'MyApp', '[]', 0, '2019-01-30 06:06:10', '2019-01-30 06:06:10', '2020-01-30 11:36:10'),
('cad30e833730bacefc2fad3801915e10e40ee9157fb227b0c3626ebde7f39a3f2e411bd35e49074d', 1, 1, 'MyApp', '[]', 0, '2019-01-31 03:34:32', '2019-01-31 03:34:32', '2020-01-31 09:04:32'),
('d12cc391f4911958deca89631901d0e7f8f52bda39b5005755ec85761c7ca4b1dea75d9f75df356d', 1, 1, 'MyApp', '[]', 0, '2019-01-31 06:08:23', '2019-01-31 06:08:23', '2020-01-31 11:38:23'),
('d37271aaddbf4fdae32819e35f48c62a9e39e662bd6acbedbdacebedc2de51e6ed06c156e0d9cabf', 1, 1, 'MyApp', '[]', 0, '2019-01-30 05:24:01', '2019-01-30 05:24:01', '2020-01-30 10:54:01'),
('d6b56c8b43734fa89945965990f287870a399f39a619b97c611f4666e36989107ace95821ea76574', 1, 1, 'MyApp', '[]', 0, '2019-01-31 01:08:01', '2019-01-31 01:08:01', '2020-01-31 06:38:01'),
('d6e5b1130fd17b731177199cc7296213a5f9140bff868f11bd755394b2e15eece0337778761f6450', 1, 1, 'MyApp', '[]', 0, '2019-01-31 05:19:56', '2019-01-31 05:19:56', '2020-01-31 10:49:56'),
('d8640be92fda0ce31106f3b1fc3969edfa0a71ed8bc1714fb7b61734e63f9ab8805ea6c7c6a25dfb', 1, 1, 'MyApp', '[]', 0, '2019-02-01 02:09:04', '2019-02-01 02:09:04', '2020-02-01 07:39:04'),
('d94630f12679ad6016242fffa429b766d9f5ceae5c0aa9ed49f09ea40122f6326966bca81166e9c7', 1, 1, 'MyApp', '[]', 1, '2019-01-30 04:43:23', '2019-01-30 04:43:23', '2020-01-30 10:13:23'),
('db8ef790b4fa43ec730f123a9943390028faef184b846c4d3ec085e846a35556f75564d04db9880d', 1, 2, NULL, '[]', 0, '2019-02-02 02:56:34', '2019-02-02 02:56:34', '2020-02-02 08:26:34'),
('dd2b6e563e7326c9855afd1faf36df63617cdb38cf577e9622efb54864a2b5724f2b0d26984ee6ee', 1, 1, 'MyApp', '[]', 0, '2019-01-31 04:34:18', '2019-01-31 04:34:18', '2020-01-31 10:04:18'),
('dd43efae0698de0d5a69adaded8f8618fcf2fc55cdf481e069b70946f2f70811c12b45f9fc15263b', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:49:28', '2019-01-31 00:49:28', '2020-01-31 06:19:28'),
('dd8bd4a1298737b81cca1b13b523eedde8baf1f37bca9f66ee1d970db7f10ecadb1fb9370035890d', 1, 1, 'MyApp', '[]', 0, '2019-01-31 00:54:33', '2019-01-31 00:54:33', '2020-01-31 06:24:33'),
('e11c936844659d4bb6521f2c33466800a91cde607b788265dcd04e50bb1a298050dcfc430d341c0f', 1, 1, 'MyApp', '[]', 0, '2019-01-30 01:18:08', '2019-01-30 01:18:08', '2020-01-30 06:48:08'),
('e3d827afd40897322ec509c3055f1e13c2da84f21daa223d4530240202291c8c15a616984029d318', 1, 1, 'MyApp', '[]', 0, '2019-02-01 02:53:47', '2019-02-01 02:53:47', '2020-02-01 08:23:47'),
('e3e9715a9ecfc7c2f9ed99986c669a9b3637e5ef737d3ffcb9c90a6627391ca392bf2bebcc78fae1', 1, 1, 'MyApp', '[]', 0, '2019-01-30 08:00:10', '2019-01-30 08:00:10', '2020-01-30 13:30:10'),
('e578e332606bb3e455f5e534a50c17e825833da9c73531ff5ac01bde1e6478f704f8bdf1240d49ca', 1, 2, NULL, '[]', 0, '2019-02-01 06:54:42', '2019-02-01 06:54:42', '2020-02-01 12:24:42'),
('ebb87acfa38ef9c9e5d52d788f90ebbee1a65b939cb9523d1c1ed70c7411d5e95100b7e8348af1f0', 1, 2, NULL, '[]', 0, '2019-02-02 00:34:16', '2019-02-02 00:34:16', '2020-02-02 06:04:16'),
('ecb98b71d562af3110024cd5305c250dc7ed48da87d0ad652111e3b64a60838edc9c86294e55cad3', 3, 1, 'MyApp', '[]', 0, '2019-01-30 06:26:29', '2019-01-30 06:26:29', '2020-01-30 11:56:29'),
('eeb93da6b0d0af1d079f5abd9c5858c64dcf676fac9cd3085f547ea670131b2cf62944bac0351d49', 9, 1, 'MyApp', '[]', 0, '2019-01-30 08:44:27', '2019-01-30 08:44:27', '2020-01-30 14:14:27'),
('eecd07378821c8d3e47abc5c5ea37d6e947bed85ec4fbcfdaa3e7d56e5107a1ff31f92338badbead', 1, 1, 'MyApp', '[]', 0, '2019-01-30 06:30:13', '2019-01-30 06:30:13', '2020-01-30 12:00:13'),
('f2a919b45f058ba48d9f21ec9590aab7a4a351f82884b0f3240f60bca36bc8ebec74df507fcefcec', 7, 1, 'MyApp', '[]', 0, '2019-01-30 08:31:37', '2019-01-30 08:31:37', '2020-01-30 14:01:37'),
('f615ee151003ef33fa419dcce5e54ad4c7eacba5cb44db0a4a4778b1ecdd1451f609d01ab7262ba5', 1, 1, 'MyApp', '[]', 0, '2019-01-31 04:28:00', '2019-01-31 04:28:00', '2020-01-31 09:58:00'),
('f8aeab271dc3845ba4fa719945dcc2c0a52af5deedf2c0be13cdfa17483ffa1e478e522ef8a10de8', 1, 1, 'MyApp', '[]', 0, '2019-01-31 05:56:51', '2019-01-31 05:56:51', '2020-01-31 11:26:51'),
('fd4c843233eed35e1d1df56613d7996b2cf54e3181294b8712117fedb8c8cd148ef2001dbb9e9db2', 1, 1, 'MyApp', '[]', 0, '2019-01-31 04:36:39', '2019-01-31 04:36:39', '2020-01-31 10:06:39'),
('ffea1684964054f85b006e7c96546b5af94729bec27e835ee209b85eea53dbbbf88592795c9374c2', 8, 1, 'MyApp', '[]', 0, '2019-01-30 08:44:15', '2019-01-30 08:44:15', '2020-01-30 14:14:15');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'ES10Ssf1mi4dvjk3WREc83FejozFl7VphWtyFl4X', 'http://localhost', 1, 0, 0, '2019-01-30 00:48:48', '2019-01-30 00:48:48'),
(2, NULL, 'Laravel Password Grant Client', 'Hbg7WacczuSOvJduG7Qt7tNMwHCJ9Gyui5BXANoN', 'http://127.0.0.1:8000', 0, 1, 0, '2019-01-30 00:48:48', '2019-01-30 00:48:48'),
(3, 1, 'test', 'yGnTPMKNrod5hKgbi8gvd2GLU4irf2EHVxWjscEF', 'http://localhost:8000/api/login', 0, 0, 0, '2019-01-31 04:32:14', '2019-01-31 04:32:14'),
(4, 1, 'client server', 'X1RHULVEN5fX6RZXdk6NnQj0RWqbcJFbpgSB5lEw', 'http://127.0.0.1:8001/callback', 0, 0, 0, '2019-01-31 08:57:27', '2019-01-31 08:57:27'),
(5, 1, 'Client Server', 'SSXW4QsNNPVyx7s89KleNBojCKmSzXKFp2meIR7d', 'http://localhost:8000/auth/callback', 0, 0, 0, '2019-02-02 00:45:00', '2019-02-02 00:45:00'),
(6, NULL, 'Client server', 'D7mVJTcWpMwB1eNiYpeKvD0GmyhwTwPEGKAEbBxP', '', 0, 0, 0, '2019-02-02 00:48:01', '2019-02-02 00:48:01');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-01-30 00:48:48', '2019-01-30 00:48:48');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('0d1b0acda879f6855f1d24e83c9522f1ef0f067843e1e071d85eae358057e38afb3d771050c6f91d', 'ebb87acfa38ef9c9e5d52d788f90ebbee1a65b939cb9523d1c1ed70c7411d5e95100b7e8348af1f0', 0, '2020-02-02 06:04:16'),
('2dfe32a8f652eaef88d98fd2af9cd6f74d4eeee559fe4436458a5be50dd751955c152cf1582e2421', 'bc74ca0f08dde86aef573484ee4ea583dba67fc6c92095778c780c6d52e6dc0bcabd173669e14d2f', 0, '2020-02-02 08:08:46'),
('627cb41581a2ad54302e7f78cbab48d5ca77f31ee544b6f840c0244246c81717318dacb72bd8009a', 'db8ef790b4fa43ec730f123a9943390028faef184b846c4d3ec085e846a35556f75564d04db9880d', 0, '2020-02-02 08:26:34'),
('73ebe110f6a48f79211b760893bd4e9cc48b1998c01781f90c6727b2e10c3134ac284e4daa66fb4b', '507f11cdcea0d8ae76a9c1ee72dbbf9a7a138446f0b3d9fe04de9cf30ba33b2248ed84717b7b0398', 0, '2020-02-02 06:51:11'),
('91fe3b849934fed0f63c54179fb1b26b4729937e6b722631a3ad82ecec1405eeb0e12e386e1a246a', 'e578e332606bb3e455f5e534a50c17e825833da9c73531ff5ac01bde1e6478f704f8bdf1240d49ca', 0, '2020-02-01 12:24:42'),
('971a50f1a8dafaf879350fd66695583af1e066d3467542f3d38e550cf199f6e83dc5282366232199', 'ac538b47a342ea2b430167d436e79476a45ce111054ed768564b7963ab55158628e8382ae051b60a', 0, '2020-02-02 06:07:02'),
('acec5eced9485545ea480d1a902a2566b3e815226d3ffa3f57345a50cb8e91fa5d9e6c8a4592594b', '8788db5706844cbaeab4da492427606e28a4e9d59c2604d3c53599c41d9b840dd8e1146b50c643ad', 0, '2020-02-01 13:30:19'),
('ddbe8f25cbbd4b7deabc15dff8eb5c2b729fe076cf7c804d9504aff56482ae73bf27c7441a2ea305', 'a24911636dc5fccd3cc6e452768a666bcc521e1111a55cca9b768a94d3a1568128866c9895cac329', 0, '2020-02-01 13:51:46'),
('eb47f0e523e2dc18bdfe77b32e7c2fa05d0bb526bbd1d0ba519569e2e6c3c85e54acbe58f90a93fe', '2dd1194f0278828f1bf7037a66a6a2fe393b09560e33fba17f22d6462461a474de7b06b6db839221', 0, '2020-02-02 06:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CompanyName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `roles`, `CompanyName`, `phone`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Govind', '', '', '', 'govindmc11@gmail.com', NULL, '$2y$10$StGz.URnYrkUGRMCdyAJs.9ZzOsgBOFUp3BvZTX1MbbD8VwZSOa8q', 'ZbE07NLavgWLMiEwDWnJx0WzCGr50UJbu48Tw6oYzTGYCvxHkHarhh2XZYaV', '2019-01-30 01:18:08', '2019-01-30 01:18:08'),
(2, 'Test', '', '', '', 'test@mail.com', NULL, '$2y$10$7BJhSwDEeNiMO1Lv2ItohuRkyAZ7iQArn/cXK/YpKzlyyj9CgTzhW', NULL, '2019-01-30 02:41:05', '2019-01-30 02:41:05'),
(3, 'ish', '', '', '', 'ish@gmail.com', NULL, '$2y$10$ryDVqJkLRuuGmjfW/11XQuUgHEDdCztGgg45AsKNtGyvMqhWS2z2G', NULL, '2019-01-30 06:26:29', '2019-01-30 06:26:29'),
(4, 'dev', '', '', '', 'dev@mail.com', NULL, '$2y$10$NQ8Ors3qbVMNiCiHajN7SuP8hjexaxDIZ4kStF67QcOyASHcbYjh2', NULL, '2019-01-30 08:17:07', '2019-01-30 08:17:07'),
(6, 'dev2', '', '', '', 'dev5@mail.com', NULL, '$2y$10$YFcwdn0vxurz0HybtmeXj.bN9iFcnHOB08cbTkIhrEkudP5MAaJwq', NULL, '2019-01-30 08:20:17', '2019-01-30 08:20:17'),
(7, 'ishanknew', '', '', '', 'ishanknew@gmail.com', NULL, '$2y$10$ICVU02hXZ0r7mxqzDbLc8OESHDaQfybJA/Gte.vdQGEx6tPLo5GX2', NULL, '2019-01-30 08:31:37', '2019-01-30 08:31:37'),
(8, 'ishnew', 'ADMIN', '', '', 'ishnew@gmail.com', NULL, '$2y$10$dm5a9pymHqScdsfi0JP4s.4/U6J/HFeQy/sSMf6gdNzd3aB.572gm', NULL, '2019-01-30 08:42:03', '2019-01-30 08:42:03'),
(9, 'Ishank Gupta1995', 'ADMIN', '', '', 'ishank1995@gmail.com1995', NULL, '$2y$10$YTJhywmLq7gael0YPJVqEuc/OamcR1504YTGv6iyB0eRX6gTsvamS', NULL, '2019-01-30 08:44:10', '2019-01-30 08:44:10'),
(14, 'Development', NULL, 'Hotel and Resort', '8976554433', 'sachin@mail.com123', NULL, '$2y$10$fe2Odvveo9Lr7PPcYB.4jubuxd.1si9YQ9ZxPz7VnVzBigd5ZO.ZC', 'sfmP91glkHVLj1KOXTQaKidZKvP4gHWonJ06onx8wWtig0u4Bvo2qLpMTHog', '2019-02-06 05:26:18', '2019-02-06 05:26:18'),
(15, 'Vineet Verma', NULL, 'smoething', '1234567890', 'vineetvrm05@gmail.com', NULL, '$2y$10$O7alvrICWcqV97GKRqZPwuam7el0GIeuZ7OgYk9hDf95lgs7vfwki', NULL, '2019-02-06 06:02:03', '2019-02-06 06:02:03'),
(16, 'Siddharth Verma', NULL, 'Sidd Hotels', '9990272451', 'sidvrm@gmail.com', NULL, '$2y$10$oj.aZ86GLxppdL9pjoo8fuZ.8DBr3ogLhtmUjtuDCJ6BtFW7sPkyW', 'Igm5Fc9Ee5qSBYIYwDL1aFRZHFeFmIcjOQDCmkA1d5tvYJE1etdpAkRPWIqW', '2019-02-07 01:47:12', '2019-02-07 01:47:12'),
(17, 'Development', NULL, 'Comany', '9723675322', 'company@mail.com', NULL, '$2y$10$0nlkqHlpIKIJYD5H1ihKDuzq0EtPzamKQCiCcidwByBJLFa.sdzra', 'r59wRp9M58LrW063Na2buEb1VJV1ufEPl7ABcJ9WCwmLdWpU8zaG5ebB8GTV', '2019-02-07 03:11:46', '2019-02-07 03:11:46'),
(18, 'Ajay', NULL, 'XYZ', '9718664122', 'ajay@mail.com', NULL, '$2y$10$ZfoTceMOdS6NKvOUyM3q4.KVrwgsbg0w/QX4OoiwjYjaDQ4cy7nhu', 'WfJULf4JkM8yGEMEapbbu6tVznG6s8UcWyacpSu1BvmX8T9P4Iw3EuKc3FhF', '2019-02-07 07:54:01', '2019-02-07 07:54:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
